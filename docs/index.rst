.. Minimal Publication with CI documentation master file, created by
   sphinx-quickstart on Sun Oct 31 12:16:38 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=========================================================
 Welcome to Minimal Publication with CI's documentation!
=========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


This is a project to showcase how to use Gitlab's CI/CD functionality
to convert EOA documents to their various output formats.

The configuration needs to be done in two different places:
* A server on which the `gitlab-runner` runs
* The Gitlab instance

Preparing Gitlab
================

Create or import the project in Gitlab. In case of this project,
everything is in place. The most important file for the pipeline is
`gitlab-ci.yml` which contains the configuration for the CI pipeline.
On top of the file it states the image that is going to be used in the
CI steps, below are the definitions for the different stages of the
workflow, what commands are run and where the output files are
deposited.

For each project, CI has to be enabled separately. In the menu on the
left, go to Settings and then General.

.. _fig:settingsgeneral:
.. figure:: /_static/settingsgeneral.png
   :scale: 50 %
   :alt: Go to Settings and then General

Then, expand the section "Visibility, project features, permissions"

.. _fig:settingsprojectfeatures:
.. figure:: /_static/settingsprojectfeatures.png
   :scale: 50 %
   :alt: Visibility, project features, permissions

And enable the features "CI/CD" and just below that "Container registry".

.. _fig:settingsenable:
.. figure:: /_static/settingsenable.png
   :scale: 50 %
   :alt: Enable CI/CD and Container registry

Make sure to save the changes by scrolling to the end of the section and clicking "Save changes"

.. _fig:settingssave:
.. figure:: /_static/settingssave.png
   :scale: 50 %
   :alt: Save changes

You should now have a new section under Settings in the menu on the
left: "CI/CD". Click on it and then expand the "Runners" section. A
runner for the whole Edition Open Access Group has been installed.

.. _fig:grouprunners:
.. figure:: /_static/grouprunners.png
   :scale: 50 %
   :alt: Save changes

This runner is already active and can be used right away. So whenever
you push changes, the pipeline will run.

====================
 Indices and tables
====================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
